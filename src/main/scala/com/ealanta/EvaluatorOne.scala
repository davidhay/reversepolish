package com.ealanta

/**
 * @author davidhay
 */
object EvaluatorOne extends ExprBuilder with ReversePolishEvaluator {

  def evaluate(reversePolish:String): Option[Double] = {
    println(s"evaluating reversePolish [$reversePolish")
    val tokens = reversePolish.split(" ").toList
    def exprs = build(tokens, List())
    exprs.headOption.map { expr =>
      expr.value
    }
  }

}

sealed trait Expr {
  def value: Double
}
case class NumberExpr(value: Double) extends Expr

case class PlusExpr(expr1: Expr, expr2: Expr) extends Expr {
  def value = expr1.value + expr2.value
}
case class MinusExpr(expr1: Expr, expr2: Expr) extends Expr {
  def value = expr1.value - expr2.value
}
case class MulExpr(expr1: Expr, expr2: Expr) extends Expr {
  def value = expr1.value * expr2.value
}
case class DivExpr(expr1: Expr, expr2: Expr) extends Expr {
  def value = expr1.value / expr2.value
}

trait ExprBuilder {
  def build(tokens: List[String], stack: List[Expr]): List[Expr] = {
    tokens match {
      case Nil => stack
      case h :: t => {
        def build1(expr: Expr) = {
          build(t, expr :: stack)
        }
        def build2(expr: Expr) = {
          build(t, expr :: stack.drop(2))
        }
        lazy val left = stack.tail.head
        lazy val right = stack.head
        h match {
          case "+"                      => build2(PlusExpr(left, right))
          case "-"                      => build2(MinusExpr(left, right))
          case "*"                      => build2(MulExpr(left, right))
          case "/"                      => build2(DivExpr(left, right))
          case n if n.forall(_.isDigit) => build1(NumberExpr(n.toDouble))
          case _                        => throw new RuntimeException(s"OOPs unexpected token [$h]")
        }
      }
    }
  }
}
