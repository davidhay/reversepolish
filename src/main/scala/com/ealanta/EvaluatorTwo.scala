package com.ealanta

/**
 * @author davidhay
 */
trait Token
case class NumberToken(value: Double) extends Token
trait OpToken extends Token {
  def fn : (Double,Double) => Double
  def eval(left: NumberToken, right: NumberToken): NumberToken = {
    NumberToken(fn(left.value,right.value))
  }
}
case object PlusToken extends OpToken {
  val fn : (Double,Double) => Double = _ + _
}
case object MulToken extends OpToken {
  val fn : (Double,Double) => Double = _ * _ 
}
case object MinusToken extends OpToken {
  val fn : (Double,Double) => Double = _ - _ 
}
case object DivToken extends OpToken {
  val fn : (Double,Double) => Double = _ / _ 
}

object EvaluatorTwo extends ReversePolishEvaluator {

  def evaluate(reversePolish: String): Option[Double] = {

    def process(tokens: List[String], stack: List[Token]): Option[Token] = {
      def addOp(op: OpToken): Option[Token] = stack match {
        case (n2 @ NumberToken(_)) :: (n1 @ NumberToken(_)) :: stacktail => process(tokens.tail, op.eval(n1, n2) :: stacktail)
        case _ => throw new RuntimeException("Encountered an operation but there was not 2 numbers on stack!")
      }
      tokens match {
        case Nil                           => stack.headOption
        case n :: t if n.forall(_.isDigit) => process(t, NumberToken(n.toDouble) :: stack)
        case "*" :: t                      => addOp(MulToken)
        case "-" :: t                      => addOp(MinusToken)
        case "/" :: t                      => addOp(DivToken)
        case "+" :: t                      => addOp(PlusToken)
        case t                             => throw new RuntimeException(s"unexpected token $t")
      }
    }

    val tokens = reversePolish.split(" ").toList

    val res:Option[Double] = process(tokens, List()) map {
      case NumberToken(value) => value
    }
    res
  }

}

