package com.ealanta

/**
 * @author davidhay
 */
trait ReversePolishEvaluator {
  def evaluate(tokens:String):Option[Double]
}