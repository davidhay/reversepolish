package com.ealanta

import org.scalatest.FlatSpec
import org.scalatest.Matchers

/**
 * @author davidhay
 */
class ReversePolishSpec extends FlatSpec with Matchers {

  def checkEvaluator(evaluator:ReversePolishEvaluator) = {
    
    s"the reverse polish evaluator ${evaluator.toString} " should "work as expected" in {
      evaluator.evaluate("14 2 /") should be (Some(7.0))      
      evaluator.evaluate("5 1 2 + 4 * + 3 -") should be (Some(14.0))      
      evaluator.evaluate("6 4 5 + * 25 2 3 + / -") should be (Some(49.0))      
    }
  }
  
  checkEvaluator(EvaluatorOne)
  checkEvaluator(EvaluatorTwo)
}

